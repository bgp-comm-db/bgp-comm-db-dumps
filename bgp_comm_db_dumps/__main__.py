'''
Created on 15.02.2015

@author: raabf
'''

if __name__ == "__main__" and __package__ is None:
    __package__ = "bgp_comm_db_dumps"  # PEP 366

# develop
import sys
sys.path.append('/home/raabf/git/bgp-comm-db-framework')

from .parser import *
from . import mrtparse as mrtparse_module
from .one_len_as_path import *
from bgp_comm_db.connection_session import ConnectionSession
from bgp_comm_db.db import *
from bgp_comm_db.environment import *
from bgp_comm_db.tools import *
from datetime import datetime as DateTime
from marshmallow import pprint
from sqlalchemy import desc
from sqlalchemy.orm import aliased
from sqlalchemy_utils.functions import *
from tabulate import tabulate
import click
import dateutil.parser
import inspect
import os
import time

from sqlalchemy import func
from sqlalchemy import distinct

from sqlalchemy.sql.expression import BinaryExpression
from sqlalchemy.sql import bindparam

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"

@click.group()
def main():
    pass

insert_opt_rollback_trans = True

@main.group()
@click.option('--rollback-trans/--no-rollback-trans', default=True,
              help="Use a special transaction "
              "which will be rolled back at the end. "
              "In the DB nothing will be changed.")
def insert(rollback_trans):
    global insert_opt_rollback_trans
    insert_opt_rollback_trans = rollback_trans


class Playing(ConnectionSession):

    """docstring for Playing"""

    def __init__(self):
        super(Playing, self).__init__(rollback_trans=True)

@main.command()
@click.argument('function')
def do(function):
    with Playing() as playing_:
        func = getattr(playing_, function)
        func()

@main.command()
@click.argument('mrtfile')
def bgpdump(mrtfile):
    bgpdump_ = BGPDump(mrtfile)
    bgpdump_.parse_()


@main.command()
@click.argument('mrtfile')
def mrtparse(mrtfile):
    mrtparse_ = MRTParse(mrtfile)
    mrtparse_.parse_()

@main.command()
@click.argument('mrtdir')
def mrtparse_statistics(mrtdir):
    mrtparse_module.statistics(mrtdir)

@main.command()
@click.argument('mrtdir')
@click.argument('dumpfile')
def mrtparse_statistics2(mrtdir, dumpfile):
    mrtparse_module.statistics2(mrtdir, dumpfile)

@main.command()
@click.argument('mrtdir')
@click.argument('dumpfile')
def mrtparse_statistics4(mrtdir, dumpfile):
    mrtparse_module.statistics4(mrtdir, dumpfile)

@main.command()
@click.argument('mrtdir')
@click.argument('dumpfile')
def mrtparse_statistics3(mrtdir, dumpfile):
    with mrtparse_module.MRTParseStatistics() as statistics:
        statistics.statistics3(mrtdir, dumpfile)

@main.command()
@click.argument('mrtdir')
@click.argument('dumpfile')
def mrtparse_make_comm_as_path_dict(mrtdir, dumpfile):
    mrtparse_module.make_comm_as_path_dict(mrtdir, dumpfile)

@main.command()
def one_see():
    with OneLenAsPath() as one_len:
        one_len.see()

if __name__ == '__main__':
    main()
