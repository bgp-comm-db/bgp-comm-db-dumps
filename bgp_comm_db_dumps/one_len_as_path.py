from bgp_comm_db.annotation.typed import optional
from bgp_comm_db.annotation.typed import typechecked
from bgp_comm_db.annotation.typed import union
from bgp_comm_db.annotation.typed import void
from bgp_comm_db.connection_session import ConnectionSession
from bgp_comm_db.db import *
from sqlalchemy.orm import aliased
from sqlalchemy.orm import eagerload
from sqlalchemy.orm import joinedload
from sqlalchemy.orm import lazyload
from sqlalchemy.orm import load_only
from sqlalchemy.orm import mapper
from sqlalchemy.orm import noload
from sqlalchemy.orm import subqueryload
from sqlalchemy.orm import with_polymorphic
from tabulate import tabulate
import mrtparse as mp
import os
import pickle
import time


class OneLenAsPath(ConnectionSession):

    def __init__(self):
        super(OneLenAsPath, self).__init__(rollback_trans=True)
        self.session = None

    def see(self):
        dumpfile = "/media/osshare-crypt/routeviews2/p3-single-one_path_len_other_comms.data"

        with open(dumpfile, 'rb') as data_file:
            one_lst = pickle.load(data_file)

        for one in one_lst:
            print("#####",one[0],"#####")
            other_print_lst = []
            for comm in one[4]:
                std_value = StdValue(comm)
                comm_entry_lst = StdComm.get_std_comm_eager_fields(
                    self.session, std_value)
                for entry in comm_entry_lst:
                    other_print_lst.append(
                        (std_value.str_value, entry.description))
            # print(other_print_lst)
            print(tabulate(other_print_lst))
