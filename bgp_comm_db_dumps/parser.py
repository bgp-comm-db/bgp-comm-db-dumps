"""
"""

# 0: TABLE_DUMP2 or TABLE_DUMP2

# -H  mode=0    multi-line, human-readable (the default)\n\
# -m  mode=1    one-line per entry with unix timestamps\n\
# -M  mode=2    one-line per entry with human readable timestamps\n\

# TABLE_DUMP:
#  1: time
#  2: 'A':Human Readable 'B': Unix timestamps
#  3: Peer IP
#  4: Peer AS (Without 'AS' prefix)
#  5: [Prefix]/[Prefix-Length]
#  6: AS Path (Space separated List (Without 'AS' prefix))
#  7: Origin: 'IGP', 'EGP', 'INCOMPLETE'
#  8: nexthop
#  9: LocalPref (npref)
# 10: MED (nmed)
# 11: Communities (space separated AS list (Without 'AS' prefix))
# 12: (Atomic) aggregate - 'AG' or 'NAG'
# 13: Aggregator AS
# 14: Aggregator Address (IP)
# 15: Empty

# TABLE_DUMP2:
#  1: time
#  2: 'A':Human Readable 'B': Unix timestamps
#  3: Peer IP
#  4: Peer AS
#  5: [Prefix]/[Prefix-Length]
#  6: AS Path (Space separated List (Without 'AS' prefix))
#  7: Origin: 'IGP', 'EGP', 'INCOMPLETE'
#  8: nexthop
#  9: LocalPref (npref)
# 10: MED (nmed)
# 11: Communities (space separated AS list (Without 'AS' prefix))
# 12: (Atomic) aggregate - 'AG' or 'NAG'
# 13: Aggregator AS
# 14: Aggregator Address (IP)

import csv
from bgp_comm_db.annotation.typed import (typechecked)
import subprocess

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"
__all__ = ['BGPDump']

class BGPDump(object):

    """docstring for BGPDump"""

    def __init__(self, mrtfile):
        super(BGPDump, self).__init__()
        self.mrtfile = mrtfile

    def parse_(self):

        bgpdump_cmd = ['bgpdump', '-s', '-m', self.mrtfile]
        with subprocess.Popen(bgpdump_cmd, stdout=subprocess.PIPE, bufsize=1,
                              universal_newlines=True) as bgpdump_ps:

            reader = csv.DictReader(bgpdump_ps.stdout, fieldnames=(
                'version', 'time', 'timeformat',
                'peer_ip', 'peer_as', 'prefix',
                'aspath', 'origin', 'nexthop',
                'localpref', 'med', 'communities',
                'aggregate', 'aggregator_as',
                'aggregator_ip'),
                delimiter='|', skipinitialspace=True)

            for i, line in enumerate(reader):
                print("###################################")
                for key, value in line.items():
                    print(key, ": ", value)

        # for i, row in enumerate(reader):
        #     row['communities'] = row['communities'].split()
        #     print(row['communities'])

                # if i > 40:
                    # break
