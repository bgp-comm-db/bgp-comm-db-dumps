"""
"""

import os
import pickle
import time
import sys

import numpy as np

import mrtparse as mp
from bgp_comm_db.connection_session import ConnectionSession
from bgp_comm_db.db import *

__author__ = "Fabian Raab"
__email__ = "Fabian Raab <fabian.raab@tum.de>"
__all__ = ['MRTParse']


class MRTParse(object):
    """docstring for MRTParse"""

    def __init__(self, mrtfile):
        super(MRTParse, self).__init__()
        self.mrtfile = mrtfile

    def parse_(self):
        reader = mp.Reader(self.mrtfile)

        for i, m in enumerate(reader):
            m = m.mrt

            if m.type == mp.MSG_T['TABLE_DUMP']:
                print('TABLE_DUMP', m)
            elif m.type == mp.MSG_T['TABLE_DUMP_V2']:
                if m.subtype == mp.TD_V2_ST['PEER_INDEX_TABLE']:
                    # print('TABLE_DUMP_V2', m.peer)
                    pass
                elif (m.subtype == mp.TD_V2_ST['RIB_IPV4_UNICAST']
                      or m.subtype == mp.TD_V2_ST['RIB_IPV4_MULTICAST']
                      or m.subtype == mp.TD_V2_ST['RIB_IPV6_UNICAST']
                      or m.subtype == mp.TD_V2_ST['RIB_IPV6_MULTICAST']):
                    # print('TABLE_DUMP_V2', m.rib)
                    for entry in m.rib.entry:
                        # print("\thi entry")
                        for attr in entry.attr:
                            # print("\t\tservus attr")
                            hi = False
                            if attr.type == mp.BGP_ATTR_T['COMMUNITY']:
                                line = '%s' % mp.val_dict(
                                    mp.BGP_ATTR_T,
                                    attr.type)
                                # print(line + ': %s' % ' '.join(attr.comm))
                                # print(attr.as_path)
                                hi = True
                            elif attr.type == mp.BGP_ATTR_T['AS_PATH']:
                                if len(attr.as_path) > 1:
                                    print(attr.as_path)
                                if hi:
                                    print("blubb")
                            elif attr.type == mp.BGP_ATTR_T['EXTENDED_COMMUNITIES']:
                                ext_comm_list = []
                                for ext_comm in attr.ext_comm:
                                    ext_comm_list.append('0x%016x' % ext_comm)
                                print(line + ': %s' % ' '.join(ext_comm_list))

            elif (m.type == mp.MSG_T['BGP4MP']
                  or m.type == mp.MSG_T['BGP4MP_ET']):
                print('BGP4MP', m)

                # if i > 0:
                #     break


STATISTICS_NUMBER_COMMS = None
STATISTICS_COMM_SET = None
STATISTICS_MAX_COMM_IN_SINGLE_MSG = None

# STATISTICS_NUMBER_AS_PATHS = None
STATISTICS_AS_PATH_SET = None

STATISTICS_NUMBER_MSG_WITH_COMM = None
STATISTICS_NUMBER_MSG = None

STATISTICS_NUMBER_EXT_COMM = None


def statistics(dir_path):
    global STATISTICS_NUMBER_COMMS
    global STATISTICS_COMM_SET
    global STATISTICS_MAX_COMM_IN_SINGLE_MSG

    # STATISTICS_NUMBER_AS_PATHS = None
    global STATISTICS_AS_PATH_SET

    global STATISTICS_NUMBER_MSG_WITH_COMM
    global STATISTICS_NUMBER_MSG

    global STATISTICS_NUMBER_EXT_COMM

    STATISTICS_NUMBER_COMMS = 0
    STATISTICS_COMM_SET = set()
    STATISTICS_MAX_COMM_IN_SINGLE_MSG = 0

    # STATISTICS_NUMBER_AS_PATHS = None
    STATISTICS_AS_PATH_SET = set()

    STATISTICS_NUMBER_MSG_WITH_COMM = 0
    STATISTICS_NUMBER_MSG = 0

    STATISTICS_NUMBER_EXT_COMM = 0

    for dir in os.listdir(dir_path):
        start = time.time()
        subdir = os.path.join(dir_path, dir)
        print(dir)
        for dump_file in os.listdir(subdir):
            startf = time.time()
            _statictics_single_dump(os.path.join(subdir, dump_file))
            endf = time.time()
            print("\t", dump_file, endf - startf)
        end = time.time()
        print("\tMONTH TOTAL TIME", end - start)

    print("STATISTICS_NUMBER_COMMS", STATISTICS_NUMBER_COMMS)
    print("STATISTICS_COMM_SET", len(STATISTICS_COMM_SET))
    print("STATISTICS_MAX_COMM_IN_SINGLE_MSG",
          STATISTICS_MAX_COMM_IN_SINGLE_MSG)
    print("")

    print("STATISTICS_AS_PATH_SET", len(STATISTICS_AS_PATH_SET))
    print("STATISTICS_NUMBER_MSG_WITH_COMM", STATISTICS_NUMBER_MSG_WITH_COMM)
    print("STATISTICS_NUMBER_MSG", STATISTICS_NUMBER_MSG)

    print("STATISTICS_NUMBER_EXT_COMM", STATISTICS_NUMBER_EXT_COMM)


def _statictics_single_dump(dump_file):
    global STATISTICS_NUMBER_COMMS
    global STATISTICS_COMM_SET
    global STATISTICS_MAX_COMM_IN_SINGLE_MSG

    # STATISTICS_NUMBER_AS_PATHS = None
    global STATISTICS_AS_PATH_SET

    global STATISTICS_NUMBER_MSG_WITH_COMM
    global STATISTICS_NUMBER_MSG

    global STATISTICS_NUMBER_EXT_COMM

    reader = mp.Reader(dump_file)

    for i, m in enumerate(reader):
        m = m.mrt

        if m.type == mp.MSG_T['TABLE_DUMP']:
            raise Exception("Type TABLE_DUMP unexpected.")
        elif m.type == mp.MSG_T['TABLE_DUMP_V2']:
            if m.subtype == mp.TD_V2_ST['PEER_INDEX_TABLE']:
                pass
            elif (m.subtype == mp.TD_V2_ST['RIB_IPV4_UNICAST']
                  or m.subtype == mp.TD_V2_ST['RIB_IPV4_MULTICAST']
                  or m.subtype == mp.TD_V2_ST['RIB_IPV6_UNICAST']
                  or m.subtype == mp.TD_V2_ST['RIB_IPV6_MULTICAST']):

                for entry in m.rib.entry:

                    STATISTICS_NUMBER_MSG += 1

                    for attr in entry.attr:

                        if attr.type == mp.BGP_ATTR_T['COMMUNITY']:
                            STATISTICS_NUMBER_MSG_WITH_COMM += 1
                            STATISTICS_MAX_COMM_IN_SINGLE_MSG = max(
                                STATISTICS_MAX_COMM_IN_SINGLE_MSG,
                                len(attr.comm))

                            for comm in attr.comm:
                                STATISTICS_NUMBER_COMMS += 1
                                STATISTICS_COMM_SET.add(comm)

                        elif attr.type == mp.BGP_ATTR_T['AS_PATH']:
                            for as_path in attr.as_path:
                                STATISTICS_AS_PATH_SET.add(as_path['val'])

                        elif attr.type == mp.BGP_ATTR_T['EXTENDED_COMMUNITIES']:
                            ext_comm_list = []
                            for ext_comm in attr.ext_comm:
                                STATISTICS_NUMBER_EXT_COMM += 1

        elif (m.type == mp.MSG_T['BGP4MP']
              or m.type == mp.MSG_T['BGP4MP_ET']):
            pass


# --------------------------------------------------------------

COMM_AS_PATH_DICT = None


def make_comm_as_path_dict(dir_path, dumpfile):
    global STATISTICS_NUMBER_COMMS
    global STATISTICS_COMM_SET
    global STATISTICS_MAX_COMM_IN_SINGLE_MSG

    # STATISTICS_NUMBER_AS_PATHS = None
    global STATISTICS_AS_PATH_SET

    global STATISTICS_NUMBER_MSG_WITH_COMM
    global STATISTICS_NUMBER_MSG

    global STATISTICS_NUMBER_EXT_COMM

    global COMM_AS_PATH_DICT

    STATISTICS_NUMBER_COMMS = 0
    STATISTICS_COMM_SET = set()
    STATISTICS_MAX_COMM_IN_SINGLE_MSG = 0

    # STATISTICS_NUMBER_AS_PATHS = None
    STATISTICS_AS_PATH_SET = set()

    STATISTICS_NUMBER_MSG_WITH_COMM = 0
    STATISTICS_NUMBER_MSG = 0

    STATISTICS_NUMBER_EXT_COMM = 0

    COMM_AS_PATH_DICT = dict()

    for dir in os.listdir(dir_path):
        start = time.time()
        subdir = os.path.join(dir_path, dir)
        print(dir)
        for dump_file in os.listdir(subdir):
            startf = time.time()
            _make_comm_as_path_dict_single_dump(
                os.path.join(
                    subdir,
                    dump_file))
            endf = time.time()
            print("\t", dump_file, endf - startf)
        end = time.time()
        print("\tMONTH TOTAL TIME", end - start)

        new_dict = dict()
        for key, value in COMM_AS_PATH_DICT.items():
            new_list = []
            for str_as_path in value:
                new_list.append(str_as_path.split(' '))
            new_dict[key] = new_list

        print("Writing pickle file to", dumpfile)
        with open(dumpfile, 'wb') as data_file:
            pickle.dump(new_dict, data_file, pickle.HIGHEST_PROTOCOL)


def _make_comm_as_path_dict_single_dump(dump_file):
    global STATISTICS_NUMBER_COMMS
    global STATISTICS_COMM_SET
    global STATISTICS_MAX_COMM_IN_SINGLE_MSG

    # STATISTICS_NUMBER_AS_PATHS = None
    global STATISTICS_AS_PATH_SET

    global STATISTICS_NUMBER_MSG_WITH_COMM
    global STATISTICS_NUMBER_MSG

    global STATISTICS_NUMBER_EXT_COMM

    global COMM_AS_PATH_DICT

    reader = mp.Reader(dump_file)

    for i, m in enumerate(reader):
        m = m.mrt

        if m.type == mp.MSG_T['TABLE_DUMP']:
            raise Exception("Type TABLE_DUMP unexpected.")
        elif m.type == mp.MSG_T['TABLE_DUMP_V2']:
            if m.subtype == mp.TD_V2_ST['PEER_INDEX_TABLE']:
                pass
            elif (m.subtype == mp.TD_V2_ST['RIB_IPV4_UNICAST']
                  or m.subtype == mp.TD_V2_ST['RIB_IPV4_MULTICAST']
                  or m.subtype == mp.TD_V2_ST['RIB_IPV6_UNICAST']
                  or m.subtype == mp.TD_V2_ST['RIB_IPV6_MULTICAST']):

                for entry in m.rib.entry:

                    STATISTICS_NUMBER_MSG += 1

                    communities_set = set()
                    as_path_set = set()

                    for attr in entry.attr:

                        if attr.type == mp.BGP_ATTR_T['COMMUNITY']:
                            STATISTICS_NUMBER_MSG_WITH_COMM += 1
                            STATISTICS_MAX_COMM_IN_SINGLE_MSG = max(
                                STATISTICS_MAX_COMM_IN_SINGLE_MSG,
                                len(attr.comm))
                            communities_set.update(attr.comm)

                            for comm in attr.comm:
                                STATISTICS_NUMBER_COMMS += 1
                                STATISTICS_COMM_SET.add(comm)

                        elif attr.type == mp.BGP_ATTR_T['AS_PATH']:
                            for as_path in attr.as_path:
                                STATISTICS_AS_PATH_SET.add(
                                    as_path['val'])

                                as_path_set.add(as_path['val'])

                        elif attr.type == mp.BGP_ATTR_T['EXTENDED_COMMUNITIES']:
                            ext_comm_list = []
                            for ext_comm in attr.ext_comm:
                                STATISTICS_NUMBER_EXT_COMM += 1

                    for comm in communities_set:

                        if comm not in COMM_AS_PATH_DICT:
                            COMM_AS_PATH_DICT[comm] = set(as_path_set)
                        else:
                            COMM_AS_PATH_DICT[comm].update(as_path_set)

        elif (m.type == mp.MSG_T['BGP4MP']
              or m.type == mp.MSG_T['BGP4MP_ET']):
            pass


def parse_comm_as_path_dict(dumpfile, new_dumpfile):
    with open(dumpfile, 'rb') as data_file:
        as_path_dict = pickle.load(data_file)

    for comm, as_path_list in as_path_dict.items():
        for as_path in as_path_list:
            pass


# ------------------------------------------------------------------------

STATISTICS2_ORIGIN_AS_COMMS = dict()
STATISTICS2_ASN_OF_COMM_AMOUNT = dict()
STATISTICS2_COMMS_PER_MESSAGE = dict()
STATISTICS2_PATH_LENGS_WITH_COMM = dict()
STATISTICS2_PATH_LENGS_NO_COMM = dict()
STATISTICS2_PATH_LENGTH_COMMS_PER_MESSAGE = dict()
STATISTICS2_ORIGIN_AS_WITH_OR_NO_COMM = dict()
STATISTICS2_ORIGIN_AS_UNIQUE_COMMS = dict()
STATISTICS2_ORIGIN_AS_PREFIX = dict()
STATISTICS2_PREFIX_COMMUNITIES = dict()
STATISTICS2_ASN_OF_COMM_PREFIX = dict()


def statistics2(dir_path, dumpfile):
    global STATISTICS2_ORIGIN_AS_COMMS
    global STATISTICS2_ASN_OF_COMM_AMOUNT
    global STATISTICS2_COMMS_PER_MESSAGE
    global STATISTICS2_PATH_LENGS_WITH_COMM
    global STATISTICS2_PATH_LENGS_NO_COMM
    global STATISTICS2_PATH_LENGTH_COMMS_PER_MESSAGE
    global STATISTICS2_ORIGIN_AS_WITH_OR_NO_COMM

    # WARNING false data does not iterate trough as-path list
    global STATISTICS2_ORIGIN_AS_UNIQUE_COMMS
    global STATISTICS2_ORIGIN_AS_PREFIX
    global STATISTICS2_PREFIX_COMMUNITIES
    global STATISTICS2_ASN_OF_COMM_PREFIX

    for dir in os.listdir(dir_path):
        start = time.time()
        subdir = os.path.join(dir_path, dir)
        print(dir)
        for dump_file in os.listdir(subdir):
            startf = time.time()
            _statistics2_single_dump(
                os.path.join(
                    subdir,
                    dump_file))
            endf = time.time()
            print("\t", dump_file, endf - startf)
        end = time.time()
        print("\tMONTH TOTAL TIME", end - start)

        save_dict = dict()

        save_dict['STATISTICS2_ORIGIN_AS_COMMS'] = STATISTICS2_ORIGIN_AS_COMMS
        save_dict[
            'STATISTICS2_ASN_OF_COMM_AMOUNT'] = STATISTICS2_ASN_OF_COMM_AMOUNT
        save_dict[
            'STATISTICS2_COMMS_PER_MESSAGE'] = STATISTICS2_COMMS_PER_MESSAGE
        save_dict[
            'STATISTICS2_PATH_LENGS_WITH_COMM'] = STATISTICS2_PATH_LENGS_WITH_COMM
        save_dict[
            'STATISTICS2_PATH_LENGS_NO_COMM'] = STATISTICS2_PATH_LENGS_NO_COMM
        save_dict[
            'STATISTICS2_PATH_LENGTH_COMMS_PER_MESSAGE'] = STATISTICS2_PATH_LENGTH_COMMS_PER_MESSAGE
        save_dict[
            'STATISTICS2_ORIGIN_AS_WITH_OR_NO_COMM'] = STATISTICS2_ORIGIN_AS_WITH_OR_NO_COMM

        # WARNING false data does not iterate trough as-path list
        save_dict[
            'STATISTICS2_ORIGIN_AS_UNIQUE_COMMS'] = STATISTICS2_ORIGIN_AS_UNIQUE_COMMS
        save_dict[
            'STATISTICS2_ORIGIN_AS_PREFIX'] = STATISTICS2_ORIGIN_AS_PREFIX
        save_dict[
            'STATISTICS2_PREFIX_COMMUNITIES'] = STATISTICS2_PREFIX_COMMUNITIES
        save_dict[
            'STATISTICS2_ASN_OF_COMM_PREFIX'] = STATISTICS2_ASN_OF_COMM_PREFIX

        print("Writing pickle file to", dumpfile)
        with open(dumpfile, 'wb') as data_file:
            pickle.dump(save_dict, data_file, pickle.HIGHEST_PROTOCOL)


def _statistics2_single_dump(dump_file):
    global STATISTICS2_ORIGIN_AS_COMMS
    global STATISTICS2_ASN_OF_COMM_AMOUNT
    global STATISTICS2_COMMS_PER_MESSAGE
    global STATISTICS2_PATH_LENGS_WITH_COMM
    global STATISTICS2_PATH_LENGS_NO_COMM
    global STATISTICS2_PATH_LENGTH_COMMS_PER_MESSAGE
    global STATISTICS2_ORIGIN_AS_WITH_OR_NO_COMM
    global STATISTICS2_ORIGIN_AS_UNIQUE_COMMS
    global STATISTICS2_ORIGIN_AS_PREFIX
    global STATISTICS2_PREFIX_COMMUNITIES
    global STATISTICS2_ASN_OF_COMM_PREFIX

    amount_paths_per_entry = dict()

    reader = mp.Reader(dump_file)

    for i, m in enumerate(reader):
        m = m.mrt

        if m.type == mp.MsgT.table_dump:
            raise Exception("Type TABLE_DUMP unexpected.")
        elif m.type == mp.MsgT.table_dump_v2:
            if m.subtype == mp.TdV2St.peer_index_table:
                pass
            elif (m.subtype == mp.TdV2St.rib_ipv4_unicast
                  or m.subtype == mp.TdV2St.rib_ipv4_multicast
                  or m.subtype == mp.TdV2St.rib_ipv6_unicast
                  or m.subtype == mp.TdV2St.rib_ipv6_multicast):

                prefix_str = str(m.rib.prefix) + '/' + str(m.rib.plen)

                for entry in m.rib.entry:

                    communities_set = set()
                    as_path_merged = []

                    for attr in entry.attr:

                        if attr.type == mp.BgpAttrT.community:

                            communities_set.update(attr.comm)

                        elif attr.type == mp.BgpAttrT.as_path:
                            for seg in attr.as_path:

                                if seg['type'] == mp.AsPathSegT.as_sequence:
                                    as_path_merged = (as_path_merged +
                                                      seg['val'].split(' '))
                                elif seg['type'] == mp.AsPathSegT.as_set:
                                    as_path_merged.append(
                                        set(seg['val'].split(' ')))
                                else:
                                    print("WARN unexpected AsPathSegT type")

                        elif attr.type == mp.BgpAttrT.as4_path:
                            print("AS4 PATH", attr.as4_path)

                        elif attr.type == mp.BgpAttrT.extended_communities:
                            print("EXTENDED COMM FOUND", attr.ext_comm)

                    amount_paths_per_entry[len(as_path_merged)] = (
                        amount_paths_per_entry.get(
                            len(as_path_merged), 0) + 1)

                    # as_path_set to list
                    as_path_list = []
                    if isinstance(as_path_merged[-1], set):
                        for orig in as_path_merged[-1]:
                            print(orig)
                            tmp = as_path_merged[:-1]
                            tmp.append(orig)
                            as_path_list.append(tmp)
                    else:
                        as_path_list.append(as_path_merged)

                    for as_path in as_path_list:
                        (amount_entries, amount_comm) = (
                            STATISTICS2_ORIGIN_AS_COMMS.get(
                                as_path[-1], (0, 0)))

                        STATISTICS2_ORIGIN_AS_COMMS[as_path[-1]] = (
                            amount_entries + 1, amount_comm + len(communities_set))

                        if communities_set:
                            STATISTICS2_PATH_LENGS_WITH_COMM[len(as_path)] = (
                                STATISTICS2_PATH_LENGS_WITH_COMM.get(
                                    len(as_path), 0) + 1)
                            with_comm, no_comm = STATISTICS2_ORIGIN_AS_WITH_OR_NO_COMM.get(
                                as_path[-1], (0, 0))
                            STATISTICS2_ORIGIN_AS_WITH_OR_NO_COMM[as_path[-1]] = (
                                (with_comm + 1, no_comm))
                        else:
                            STATISTICS2_PATH_LENGS_NO_COMM[len(as_path)] = (
                                STATISTICS2_PATH_LENGS_NO_COMM.get(
                                    len(as_path), 0) + 1)
                            with_comm, no_comm = STATISTICS2_ORIGIN_AS_WITH_OR_NO_COMM.get(
                                as_path[-1], (0, 0))
                            STATISTICS2_ORIGIN_AS_WITH_OR_NO_COMM[as_path[-1]] = (
                                (with_comm, no_comm + 1))

                        as_path_dict = STATISTICS2_PATH_LENGTH_COMMS_PER_MESSAGE.get(
                            len(as_path), dict())
                        amount_comm = as_path_dict.get(len(communities_set), 0)
                        as_path_dict[len(communities_set)] = amount_comm + 1
                        STATISTICS2_PATH_LENGTH_COMMS_PER_MESSAGE[
                            len(as_path)] = as_path_dict

                        tmp = STATISTICS2_ORIGIN_AS_PREFIX.get(
                            as_path[-1], set())
                        tmp.add(prefix_str)
                        STATISTICS2_ORIGIN_AS_PREFIX[as_path[-1]] = tmp

                    as_done = set()
                    for comm in communities_set:
                        (asn, _) = StdValue.parse_value(comm)
                        (amount_entries, amount_comm) = (
                            STATISTICS2_ASN_OF_COMM_AMOUNT.get(asn, (0, 0)))
                        inc = 0
                        if asn not in as_done:
                            inc = 1
                            as_done.add(asn)
                            tmp = STATISTICS2_ASN_OF_COMM_PREFIX.get(
                                asn, set())
                            tmp.add(prefix_str)
                            STATISTICS2_ASN_OF_COMM_PREFIX[asn] = tmp

                        STATISTICS2_ASN_OF_COMM_AMOUNT[asn] = (
                            amount_entries + inc, amount_comm + len(as_path_merged))

                        tmp = STATISTICS2_ORIGIN_AS_UNIQUE_COMMS.get(
                            as_path[-1], set())
                        tmp.add(comm)
                        STATISTICS2_ORIGIN_AS_UNIQUE_COMMS[as_path[-1]] = tmp
                        # WARNING false data does not iterate trough as-path
                        # list

                    STATISTICS2_COMMS_PER_MESSAGE[len(communities_set)] = (
                        STATISTICS2_COMMS_PER_MESSAGE.
                        get(len(communities_set), 0) + len(as_path_merged))

                    tmp = STATISTICS2_PREFIX_COMMUNITIES.get(prefix_str, set())
                    tmp.update(communities_set)
                    STATISTICS2_PREFIX_COMMUNITIES[prefix_str] = tmp

        elif (m.type == mp.MsgT.bgp4mp or m.type == mp.MsgT.bgp4mp_et):
            pass

    print("amount_paths_per_entry", amount_paths_per_entry)


# ---------------------------------------------------------------

OREGON_NEIGHBOUR_ASES = [
    286, 293, 852, 1221, 1239, 1299, 1668, 2152, 2497, 2905,
    2914, 3130, 3257, 3277, 3303, 3356, 3549, 3561, 3741, 5413,
    6539, 6762, 6939, 7018, 7660, 8492, 11537, 11686, 13030,
    20912, 22388, 22652, 23673, 37100, 40191, 53364, 58511,
    62567, 200130, 202018, 393406,
]

OREGON_NEIGHBOUR_DOUBLE_PEER_PER_AS_SECOND_INDEXES = [
    26,  # AS852 (no prefix with 27)
    47,  # AS6453 (no prefix with 47 or 39)
    30,  # AS3741 (no prefix with 31)
    7,  # AS3549
    24,  # AS3130
    19,  # AS2914 (no prefix with 18)
    21,  # AS286 (no prefix with 20)
    9,  # AS1299  (no prefix with 10)
]

STATISTICS4_NEIGHBOUR_AMOUNT_PREFIXES_LIST = dict()
STATISTICS4_PATH_LENGTH_AMOUNT_PREFIXES_LIST = dict()
STATISTICS4_ORIGIN_AMOUNT_PREFIXES_LIST = dict()
STATISTICS4_ORIGIN_AMOUNT_COMMS_MIN_ARITHMETIC_MEAN_MAX = dict()
STATISTICS4_COMM_COUNT = dict()
STATISTICS4_AMOUNT_COMM_DICT_ORIGIN_FIRST_16BIT_DICT_PREFIX_SIZE_AMOUNT_DICT = dict()
STATISTICS4_COMM_AS_PATH_PREFIX_SIZE_TUPLE_AMOUNT_DICT = dict()
STATISTICS4_COMM_AS_PATH_TUPLE_AMOUNT_DICT = dict()
STATISTICS4_NEIGHBOUR_AMOUNT_AMOUNT_COMMS_DICT = dict()
STATISTICS4_NEIGHBOUR_TAXO_COUNT_DICT = dict()
STATISTICS4_NEIGHBOUR_PATH_LENGTH_AMOUNT_COMM = dict()
STATISTICS4_PATH_LENGTH_AMOUNT_COMM = dict()
STATISTICS4_PATH_LENGTH_AMOUNT_PREFIXES_LIST_AS22388 = dict()


def statistics4(dir_path, dumpfile):
    global STATISTICS4_NEIGHBOUR_AMOUNT_PREFIXES_LIST
    global STATISTICS4_PATH_LENGTH_AMOUNT_PREFIXES_LIST
    global STATISTICS4_ORIGIN_AMOUNT_PREFIXES_LIST
    global STATISTICS4_ORIGIN_AMOUNT_COMMS_MIN_ARITHMETIC_MEAN_MAX
    global STATISTICS4_COMM_COUNT
    global STATISTICS4_AMOUNT_COMM_DICT_ORIGIN_FIRST_16BIT_DICT_PREFIX_SIZE_AMOUNT_DICT
    global STATISTICS4_COMM_AS_PATH_PREFIX_SIZE_TUPLE_AMOUNT_DICT
    global STATISTICS4_NEIGHBOUR_AMOUNT_AMOUNT_COMMS_DICT
    global STATISTICS4_NEIGHBOUR_TAXO_COUNT_DICT
    global STATISTICS4_NEIGHBOUR_PATH_LENGTH_AMOUNT_COMM
    global STATISTICS4_PATH_LENGTH_AMOUNT_COMM
    global STATISTICS4_PATH_LENGTH_AMOUNT_PREFIXES_LIST_AS22388

    for dir in os.listdir(dir_path):
        start = time.time()
        subdir = os.path.join(dir_path, dir)
        print(dir)
        for dump_file in os.listdir(subdir):
            startf = time.time()
            _statistics4_single_dump(
                os.path.join(subdir, dump_file))
            endf = time.time()
            print("\t", dump_file, endf - startf)
        end = time.time()
        print("\tMONTH TOTAL TIME", end - start)

        save_dict = dict()
        save_dict2 = dict()
        save_dict3 = dict()

        save_dict[
            "STATISTICS4_NEIGHBOUR_AMOUNT_PREFIXES_LIST"] = STATISTICS4_NEIGHBOUR_AMOUNT_PREFIXES_LIST
        save_dict[
            "STATISTICS4_PATH_LENGTH_AMOUNT_PREFIXES_LIST"] = STATISTICS4_PATH_LENGTH_AMOUNT_PREFIXES_LIST
        save_dict[
            "STATISTICS4_ORIGIN_AMOUNT_PREFIXES_LIST"] = STATISTICS4_ORIGIN_AMOUNT_PREFIXES_LIST
        save_dict["STATISTICS4_ORIGIN_AMOUNT_COMMS_MIN_ARITHMETIC_MEAN_MAX"
        ] = STATISTICS4_ORIGIN_AMOUNT_COMMS_MIN_ARITHMETIC_MEAN_MAX
        save_dict["STATISTICS4_COMM_COUNT"] = STATISTICS4_COMM_COUNT
        save_dict["STATISTICS4_AMOUNT_COMM_DICT_ORIGIN_FIRST_16BIT_DICT_PREFIX_SIZE_AMOUNT_DICT"
        ] = STATISTICS4_AMOUNT_COMM_DICT_ORIGIN_FIRST_16BIT_DICT_PREFIX_SIZE_AMOUNT_DICT
        save_dict["STATISTICS4_NEIGHBOUR_AMOUNT_AMOUNT_COMMS_DICT"] = STATISTICS4_NEIGHBOUR_AMOUNT_AMOUNT_COMMS_DICT
        save_dict["STATISTICS4_NEIGHBOUR_TAXO_COUNT_DICT"] = STATISTICS4_NEIGHBOUR_TAXO_COUNT_DICT
        save_dict["STATISTICS4_NEIGHBOUR_PATH_LENGTH_AMOUNT_COMM"] = STATISTICS4_NEIGHBOUR_PATH_LENGTH_AMOUNT_COMM
        save_dict["STATISTICS4_PATH_LENGTH_AMOUNT_COMM"] = STATISTICS4_PATH_LENGTH_AMOUNT_COMM
        save_dict["STATISTICS4_PATH_LENGTH_AMOUNT_PREFIXES_LIST_AS22388"] = STATISTICS4_PATH_LENGTH_AMOUNT_PREFIXES_LIST_AS22388

        save_dict2["STATISTICS4_COMM_NEIGHBOUR_ORIGIN_PREFIX_SIZE_TUPLE_AMOUNT_LIST"
        ] = list(STATISTICS4_COMM_AS_PATH_PREFIX_SIZE_TUPLE_AMOUNT_DICT)

        save_dict3["STATISTICS4_COMM_AS_PATH_TUPLE_AMOUNT_LIST"] = list(STATISTICS4_COMM_AS_PATH_TUPLE_AMOUNT_DICT)

        print("Writing pickle file to", dumpfile)
        with open(dumpfile, 'wb') as data_file:
            pickle.dump(save_dict, data_file, pickle.HIGHEST_PROTOCOL)

        dumpfile2 = dumpfile + "-comm_path_prefix_amount.data"
        # TODO uncomment if needed
        # print("Writing pickle file to", dumpfile2)
        # with open(dumpfile2, 'wb') as data_file:
        #     pickle.dump(save_dict2, data_file, pickle.HIGHEST_PROTOCOL)

        dumpfile3 = dumpfile + "-comm_path_amount.data"
        # TODO uncomment if needed
        print("Writing pickle file to", dumpfile3)
        with open(dumpfile3, 'wb') as data_file:
            pickle.dump(save_dict3, data_file, pickle.HIGHEST_PROTOCOL)


def _statistics4_single_dump(dump_file):
    global STATISTICS4_NEIGHBOUR_AMOUNT_PREFIXES_LIST
    global STATISTICS4_PATH_LENGTH_AMOUNT_PREFIXES_LIST
    global STATISTICS4_ORIGIN_AMOUNT_PREFIXES_LIST
    global STATISTICS4_ORIGIN_AMOUNT_COMMS_MIN_ARITHMETIC_MEAN_MAX
    global STATISTICS4_AMOUNT_COMM_DICT_ORIGIN_FIRST_16BIT_DICT_PREFIX_SIZE_AMOUNT_DICT
    global STATISTICS4_COMM_AS_PATH_PREFIX_SIZE_TUPLE_AMOUNT_DICT
    global STATISTICS4_NEIGHBOUR_AMOUNT_AMOUNT_COMMS_DICT
    global STATISTICS4_NEIGHBOUR_TAXO_COUNT_DICT
    global STATISTICS4_NEIGHBOUR_PATH_LENGTH_AMOUNT_COMM
    global STATISTICS4_PATH_LENGTH_AMOUNT_PREFIXES_LIST_AS22388

    home = os.path.expanduser("~")
    dumpfile = os.path.join(home, "routeviews2/p3-single-stat3/p3-single-stat3-improved-priv-bugfix.data")

    with open(dumpfile, 'rb') as data_file:
        list_dicts1 = pickle.load(data_file)

    comm_taxo_amount = list_dicts1[
        "STATISTICS3_COMM_TAXO_AMOUNT"]
    scope_count = list_dicts1[
        "STATISTICS3_SCOPE_COUNT"]
    comm2taxo = list_dicts1["STATISTICS3_COMM2TAXO"]
    taxo2comm = list_dicts1["STATISTICS3_TAXO2COMM"]

    origin_arith = dict()

    global OREGON_NEIGHBOUR_ASES
    global OREGON_NEIGHBOUR_DOUBLE_PEER_PER_AS_SECOND_INDEXES

    one_path_len_other_comms = []

    reader = mp.Reader(dump_file)
    j_byte = 0
    for i, m in enumerate(reader):
        m = m.mrt

        # TODO remove
        if m.type == mp.MsgT.table_dump:
            raise Exception("Type TABLE_DUMP unexpected.")
        elif m.type == mp.MsgT.table_dump_v2:
            if m.subtype == mp.TdV2St.peer_index_table:
                pass
            elif (m.subtype == mp.TdV2St.rib_ipv4_unicast
                  or m.subtype == mp.TdV2St.rib_ipv4_multicast
                  or m.subtype == mp.TdV2St.rib_ipv6_unicast
                  or m.subtype == mp.TdV2St.rib_ipv6_multicast):

                prefix_str = str(m.rib.prefix) + '/' + str(m.rib.plen)

                for entry in m.rib.entry:

                    communities_set = set()
                    as_path_merged = []
                    attr_as_path = None
                    for attr in entry.attr:

                        if attr.type == mp.BgpAttrT.community:

                            communities_set.update(attr.comm)

                        elif attr.type == mp.BgpAttrT.as_path:
                            attr_as_path = attr
                            for seg in attr.as_path:

                                if seg['type'] == mp.AsPathSegT.as_sequence:
                                    as_path_merged = (
                                        as_path_merged +
                                        list(map(
                                            int, seg['val'].split(' '))))
                                elif seg['type'] == mp.AsPathSegT.as_set:
                                    as_path_merged.append(
                                        frozenset(map(
                                            int, seg['val'].split(' '))))
                                else:
                                    print(
                                        "WARN unexpected AsPathSegT type")

                        elif attr.type == mp.BgpAttrT.as4_path:
                            print("AS4 PATH", attr.as4_path)
                            raise Exception()

                        elif attr.type == mp.BgpAttrT.extended_communities:
                            print("EXTENDED COMM FOUND", attr.ext_comm)

                    as_path_list = []
                    if isinstance(as_path_merged[-1], frozenset):
                        for orig in as_path_merged[-1]:
                            tmp = as_path_merged[:-1]
                            tmp.append(orig)
                            as_path_list.append(tuple(tmp))
                    else:
                        as_path_list.append(tuple(as_path_merged))

                    communities_list = []
                    for comm in communities_set:
                        communities_list.append(StdValue(comm))

                    for as_path in as_path_list:
                        neigbour_as = int(as_path[0])
                        if entry.peer_index in OREGON_NEIGHBOUR_DOUBLE_PEER_PER_AS_SECOND_INDEXES:
                            neigbour_as_signed = -neigbour_as
                        else:
                            neigbour_as_signed = neigbour_as

                        sec_neighbour_as_set = set()

                        try:
                            if isinstance(as_path[1], set):
                                for asn_ in as_path[1]:
                                    sec_neighbour_as_set.add(int(asn_))
                            else:
                                sec_neighbour_as_set.add(int(as_path[1]))
                        except IndexError as _:
                            pass

                        # if neigbour_as not in OREGON_NEIGHBOUR_ASES:
                        #     print(neigbour_as, as_path_set, attr_as_path.as_path)
                        origin_as = int(as_path[-1])

                        # STATISTICS4_NEIGHBOUR_PATH_LENGTH_AMOUNT_COMM
                        tmp = STATISTICS4_NEIGHBOUR_PATH_LENGTH_AMOUNT_COMM.get(neigbour_as_signed, dict())
                        tmp_dict = tmp.get(len(as_path), dict())
                        tmp_dict[len(communities_set)] = tmp_dict.get(len(communities_set), 0) + 1
                        tmp[len(as_path)] = tmp_dict
                        STATISTICS4_NEIGHBOUR_PATH_LENGTH_AMOUNT_COMM[neigbour_as_signed] = tmp

                        # STATISTICS4_PATH_LENGTH_AMOUNT_COMM
                        STATISTICS4_PATH_LENGTH_AMOUNT_COMM[len(as_path)] = STATISTICS4_PATH_LENGTH_AMOUNT_COMM.get(
                            len(as_path), 0) + 1

                        # STATISTICS4_AMOUNT_COMM_DICT_ORIGIN_FIRST_16BIT_DICT_PREFIX_SIZE_AMOUNT_DICT
                        pl_origin_dict, pl_first16_dict = (
                            STATISTICS4_AMOUNT_COMM_DICT_ORIGIN_FIRST_16BIT_DICT_PREFIX_SIZE_AMOUNT_DICT.get(
                                len(communities_set), (dict(), dict())
                            )
                        )

                        tmp = pl_origin_dict.get(origin_as, dict())
                        tmp_int = tmp.get(m.rib.plen, 0)
                        tmp_int += 1
                        tmp[m.rib.plen] = tmp_int
                        pl_origin_dict[origin_as] = tmp

                        # STATISTICS4_ORIGIN_AMOUNT_COMMS_MIN_ARITHMETIC_MEAN_MAX
                        tmp = origin_arith.get(origin_as, list())
                        tmp.append(len(communities_set))
                        origin_arith[origin_as] = tmp

                        # STATISTICS4_NEIGHBOUR_AMOUNT_PREFIXES_LIST
                        # STATISTICS4_PATH_LENGTH_AMOUNT_PREFIXES_LIST
                        # STATISTICS4_ORIGIN_AMOUNT_PREFIXES_LIST
                        nap = STATISTICS4_NEIGHBOUR_AMOUNT_PREFIXES_LIST.get(
                            neigbour_as_signed, [0, 0, 0, 0, 0, 0, 0])
                        plap = STATISTICS4_PATH_LENGTH_AMOUNT_PREFIXES_LIST.get(
                            len(as_path), [0, 0, 0, 0, 0, 0, 0])
                        oap = STATISTICS4_ORIGIN_AMOUNT_PREFIXES_LIST.get(
                            origin_as, [0, 0, 0, 0, 0, 0, 0])
                        plap22388 = STATISTICS4_PATH_LENGTH_AMOUNT_PREFIXES_LIST_AS22388.get(
                            len(as_path), [0, 0, 0, 0, 0, 0, 0])
                        PRIVATE_COMM = 0
                        NEIGHBOUR_COMM = 1
                        OTHER_COMM = 2
                        NO_COMM = 3
                        SEC_NEIGHBOUR_COMM = 4
                        ALL_COMM = 5
                        ALL_PREFIX = 6

                        has_neighbour_comm = False
                        has_sec_neighbour_comm = False
                        has_private_comm = False
                        has_other_comm = False
                        # if communities_list:
                        #     print(neigbour_as, communities_set)
                        neighbour_comm_set = set()
                        sec_neighbour_comm_set = set()
                        reserved_comm_set = set()
                        other_comm_set = set()

                        # STATISTICS4_NEIGHBOUR_TAXO_COUNT_DICT
                        (am_tag, am_pref, am_black, am_anno, am_prep, am_prefix, am_classified, am_never_classified
                         ) = STATISTICS4_NEIGHBOUR_TAXO_COUNT_DICT.get(neigbour_as_signed, (0, 0, 0, 0, 0, 0, 0, 0))

                        plus_tag = 0
                        plus_pref = 0
                        plus_black = 0
                        plus_anno = 0
                        plus_prep = 0
                        plus_classified = 0
                        plus_never_classified = 0

                        never_classified = False
                        if communities_list:
                            never_classified = True # We are only interested  if the prefix has min one comm

                        for std_value in communities_list:
                            # STATISTICS4_COMM_NEIGHBOUR_ORIGIN_PREFIX_SIZE_TUPLE_AMOUNT_DICT

                            tup = (std_value.value, as_path, m.rib.prefix, int(m.rib.plen))
                            tmp_amount = STATISTICS4_COMM_AS_PATH_PREFIX_SIZE_TUPLE_AMOUNT_DICT.get(tup, 0)
                            tmp_amount += 1
                            # TODO uncomment if needed
                            # STATISTICS4_COMM_AS_PATH_PREFIX_SIZE_TUPLE_AMOUNT_DICT[tup] = tmp_amount

                            # STATISTICS4_COMM_AS_PATH_TUPLE_AMOUNT_DICT
                            tup = (std_value.value, as_path)
                            tmp_amount = STATISTICS4_COMM_AS_PATH_TUPLE_AMOUNT_DICT.get(tup, 0)
                            tmp_amount += 1
                            STATISTICS4_COMM_AS_PATH_TUPLE_AMOUNT_DICT[tup] = tmp_amount


                            # STATISTICS4_AMOUNT_COMM_DICT_ORIGIN_FIRST_16BIT_DICT_PREFIX_SIZE_AMOUNT_DICT
                            tmp = pl_first16_dict.get(std_value.asn, dict())
                            tmp_int = tmp.get(m.rib.plen, 0)
                            tmp_int += 1
                            tmp[m.rib.plen] = tmp_int
                            pl_first16_dict[std_value.asn] = tmp

                            # STATISTICS4_COMM_COUNT
                            tmp = STATISTICS4_COMM_COUNT.get(std_value.value, 0)
                            tmp += 1
                            STATISTICS4_COMM_COUNT[std_value.value] = tmp

                            if std_value.asn == neigbour_as:
                                has_neighbour_comm = True
                                neighbour_comm_set.add(std_value.value)
                            if std_value.asn in sec_neighbour_as_set:
                                has_sec_neighbour_comm = True
                                sec_neighbour_comm_set.add(std_value.value)
                            if std_value.is_reserved:
                                has_private_comm = True
                                reserved_comm_set.add(std_value.value)
                            if not ((std_value.asn == neigbour_as)
                                    or (std_value.asn in sec_neighbour_as_set)
                                    or (std_value.is_reserved)):
                                has_other_comm = True
                                other_comm_set.add(std_value.value)

                            # STATISTICS4_NEIGHBOUR_TAXO_COUNT_DICT

                            set_ = comm2taxo.get(std_value.value, set())
                            classified = False
                            for t, _ in set_:
                                if t == "tagging":
                                    plus_tag = min(plus_tag + 1, 1)
                                    classified = True
                                    never_classified = False
                                if t == "localpref":
                                    plus_pref = min(plus_pref + 1, 1)
                                    classified = True
                                    never_classified = False
                                if t == "blackhole":
                                    plus_black = min(plus_black + 1, 1)
                                    classified = True
                                    never_classified = False
                                if t == "do announce" or t == 'do not announce':
                                    plus_anno = min(plus_anno + 1, 1)
                                    classified = True
                                    never_classified = False
                                if t == "prepend":
                                    plus_prep = min(plus_prep + 1, 1)
                                    classified = True
                                    never_classified = False
                            if not classified:
                                plus_classified = min(plus_classified+1, 1)
                        if never_classified:
                            plus_never_classified = min(plus_never_classified+1, 1)


                        # STATISTICS4_NEIGHBOUR_TAXO_COUNT_DICT
                        STATISTICS4_NEIGHBOUR_TAXO_COUNT_DICT[neigbour_as_signed] = (
                            am_tag + plus_tag, am_pref + plus_pref, am_black + plus_black, am_anno + plus_anno,
                            am_prep + plus_prep, am_prefix + 1, am_classified + plus_classified,
                            am_never_classified + plus_never_classified)

                        if has_other_comm:
                            nap[OTHER_COMM] += 1
                            plap[OTHER_COMM] += 1
                            oap[OTHER_COMM] += 1
                            if entry.peer_index == 33:
                                plap22388[OTHER_COMM] += 1

                            if len(as_path) <= 1:
                                one_path_len_other_comms.append(
                                    (as_path,
                                     neighbour_comm_set,
                                     sec_neighbour_comm_set,
                                     reserved_comm_set,
                                     other_comm_set))
                        # elif has_private_comm:
                        if has_private_comm:
                            nap[PRIVATE_COMM] += 1
                            plap[PRIVATE_COMM] += 1
                            oap[PRIVATE_COMM] += 1
                            if entry.peer_index == 33: # 33 is AS22388
                                plap22388[PRIVATE_COMM] += 1
                        # elif has_sec_neighbour_comm:
                        if has_sec_neighbour_comm:
                            nap[SEC_NEIGHBOUR_COMM] += 1
                            plap[SEC_NEIGHBOUR_COMM] += 1
                            oap[SEC_NEIGHBOUR_COMM] += 1
                            if entry.peer_index == 33: # 33 is AS22388
                                plap22388[SEC_NEIGHBOUR_COMM] += 1
                        # elif has_neighbour_comm:
                        if has_neighbour_comm:
                            nap[NEIGHBOUR_COMM] += 1
                            plap[NEIGHBOUR_COMM] += 1
                            oap[NEIGHBOUR_COMM] += 1
                            if entry.peer_index == 33: # 33 is AS22388
                                plap22388[NEIGHBOUR_COMM] += 1
                        if not communities_list:
                            nap[NO_COMM] += 1
                            plap[NO_COMM] += 1
                            oap[NO_COMM] += 1
                            if entry.peer_index == 33: # 33 is AS22388
                                plap22388[NO_COMM] += 1
                        if has_other_comm or has_private_comm or has_sec_neighbour_comm or has_neighbour_comm:
                            nap[ALL_COMM] += 1
                            plap[ALL_COMM] += 1
                            oap[ALL_COMM] += 1
                            if entry.peer_index == 33: # 33 is AS22388
                                plap22388[ALL_COMM] += 1

                        nap[ALL_PREFIX] += 1
                        plap[ALL_PREFIX] += 1
                        oap[ALL_PREFIX] += 1
                        if entry.peer_index == 33: # 33 is AS22388
                            plap22388[ALL_PREFIX] += 1

                        STATISTICS4_NEIGHBOUR_AMOUNT_PREFIXES_LIST[
                            neigbour_as_signed] = nap
                        STATISTICS4_PATH_LENGTH_AMOUNT_PREFIXES_LIST[
                            len(as_path)] = plap
                        STATISTICS4_ORIGIN_AMOUNT_PREFIXES_LIST[
                            origin_as] = oap
                        STATISTICS4_PATH_LENGTH_AMOUNT_PREFIXES_LIST_AS22388[
                            len(as_path)] = plap22388

                        # STATISTICS4_AMOUNT_COMM_DICT_ORIGIN_FIRST_16BIT_DICT_PREFIX_SIZE_AMOUNT_DICT
                        STATISTICS4_AMOUNT_COMM_DICT_ORIGIN_FIRST_16BIT_DICT_PREFIX_SIZE_AMOUNT_DICT[len(communities_set)] = (
                            pl_origin_dict, pl_first16_dict
                        )

                        # STATISTICS4_NEIGHBOUR_AMOUNT_AMOUNT_COMMS_DICT
                        n_dict, sn_dict, r_dict, o_dict, a_dict = STATISTICS4_NEIGHBOUR_AMOUNT_AMOUNT_COMMS_DICT.get(
                            neigbour_as_signed, (dict(), dict(), dict(), dict(), dict()))
                        tmp = n_dict.get(len(neighbour_comm_set), 0)
                        tmp += 1
                        n_dict[len(neighbour_comm_set)] = tmp

                        tmp = sn_dict.get(len(sec_neighbour_comm_set), 0)
                        tmp += 1
                        sn_dict[len(sec_neighbour_comm_set)] = tmp

                        tmp = r_dict.get(len(reserved_comm_set), 0)
                        tmp += 1
                        r_dict[len(reserved_comm_set)] = tmp

                        tmp = o_dict.get(len(other_comm_set), 0)
                        tmp += 1
                        o_dict[len(other_comm_set)] = tmp

                        tmp = a_dict.get(len(communities_set), 0)
                        tmp += 1
                        a_dict[len(communities_set)] = tmp

                        STATISTICS4_NEIGHBOUR_AMOUNT_AMOUNT_COMMS_DICT[neigbour_as_signed] = (
                            n_dict, sn_dict, r_dict, o_dict, a_dict
                        )

                size = sys.getsizeof(STATISTICS4_COMM_AS_PATH_PREFIX_SIZE_TUPLE_AMOUNT_DICT)
                if j_byte < size:
                    print("STATISTICS4_COMM_NEIGHBOUR_ORIGIN_PREFIX_SIZE_TUPLE_AMOUNT_DICT:", size / (1024 * 1024),
                          "MB")
                    j_byte = round(size) + 1024 * 1024

        elif (m.type == mp.MsgT.bgp4mp or m.type == mp.MsgT.bgp4mp_et):
            pass

    # dumpfile = "/media/osshare-crypt/routeviews2/p3-single-one_path_len_other_comms.data"
    # print("Writing pickle file to", dumpfile)
    # with open(dumpfile, 'wb') as data_file:
    #     pickle.dump(
    #         one_path_len_other_comms,
    #         data_file,
    #         pickle.HIGHEST_PROTOCOL)

    print("calculate mean")
    for k, v in origin_arith.items():
        STATISTICS4_ORIGIN_AMOUNT_COMMS_MIN_ARITHMETIC_MEAN_MAX[k] = (min(v), np.mean(v), max(v))


# ---------------------------------------------------------------

STATISTICS3_NUMBER_EXT_COMM = 0
STATISTICS3_COMM_TAXO_AMOUNT = {
    "localpref": 0,
    "do announce": 0,
    "do not announce": 0,
    "blackhole": 0,
    "prepend": 0,
    "tagging": 0,
    "nothing": 0,
    "unknown": 0,
}
STATISTICS3_LOCALPREF_DICT = None
STATISTICS3_ANNOUNCEMENT_DICT = None
STATISTICS3_PREPEND_DICT = None
STATISTICS3_TAGGING_DICT = None
STATISTICS3_GEOGRAPHIC_DICT = None
STATISTICS3_IXP_DICT = None
STATISTICS3_PEERTYPE_DICT = None
STATISTICS3_ASES_DICT = None
STATISTICS3_SCOPE_COUNT = {
    "localpref": {'geo': dict(), 'ixp': dict(), 'peer': dict(), 'ases': dict()},
    "do announce": {'geo': dict(), 'ixp': dict(), 'peer': dict(), 'ases': dict()},
    "do not announce": {'geo': dict(), 'ixp': dict(), 'peer': dict(), 'ases': dict()},
    "blackhole": {'geo': dict(), 'ixp': dict(), 'peer': dict(), 'ases': dict()},
    "prepend": {'geo': dict(), 'ixp': dict(), 'peer': dict(), 'ases': dict()},
    "tagging": {'geo': dict(), 'ixp': dict(), 'peer': dict(), 'ases': dict()},
}
STATISTICS3_COMM2TAXO = dict()
STATISTICS3_TAXO2COMM = {
    "localpref": set(),
    "do announce": set(),
    "do not announce": set(),
    "blackhole": set(),
    "prepend": set(),
    "tagging": set(),
    "nothing": set(),
    "unknown": set(),
}
STATISTICS3_NEIGHBOUR_RESERVED_DISTANCE_DICT = dict()


class MRTParseStatistics(ConnectionSession):
    def __init__(self):
        super(MRTParseStatistics, self).__init__(rollback_trans=True)
        self.session = None

    def statistics3(self, dir_path, dumpfile):

        global STATISTICS3_NUMBER_EXT_COMM
        global STATISTICS3_COMM_TAXO_AMOUNT
        global STATISTICS3_SCOPE_COUNT
        global STATISTICS3_COMM2TAXO
        global STATISTICS3_NEIGHBOUR_RESERVED_DISTANCE_DICT

        for dir in os.listdir(dir_path):
            start = time.time()
            subdir = os.path.join(dir_path, dir)
            print(dir)
            for dump_file in os.listdir(subdir):
                startf = time.time()
                self._statistics3_single_dump(
                    os.path.join(
                        subdir,
                        dump_file))
                endf = time.time()
                print("\t", dump_file, endf - startf)
            end = time.time()
            print("\tDIR", dir, "TOTAL TIME", end - start)

            save_dict = dict()

            save_dict['STATISTICS3_COMM_TAXO_AMOUNT'] = STATISTICS3_COMM_TAXO_AMOUNT
            save_dict['STATISTICS3_SCOPE_COUNT'] = STATISTICS3_SCOPE_COUNT
            save_dict['STATISTICS3_COMM2TAXO'] = STATISTICS3_COMM2TAXO
            save_dict['STATISTICS3_TAXO2COMM'] = STATISTICS3_TAXO2COMM
            save_dict['STATISTICS3_NEIGHBOUR_RESERVED_DISTANCE_DICT'] = STATISTICS3_NEIGHBOUR_RESERVED_DISTANCE_DICT

            print("STATISTICS3_NUMBER_EXT_COMM:", STATISTICS3_NUMBER_EXT_COMM)

            print("Writing pickle file to", dumpfile)
            with open(dumpfile, 'wb') as data_file:
                pickle.dump(save_dict, data_file, pickle.HIGHEST_PROTOCOL)

    def _count_taxonomies(self, std_comm_lst: [], dict_: {}) -> bool:
        """ NOT FULLY CORRECT
        @deprecated
        """

        if not std_comm_lst:
            return False

        something_found = False

        for std_comm in std_comm_lst:

            if isinstance(std_comm, StdCommFieldValue):
                for std_field in std_comm.fields:

                    if std_field.localprefs:
                        dict_['localpref'] += 1
                        something_found = True
                    if std_field.announcements:
                        dict_['announcement'] += 1
                        something_found = True
                    if std_field.prepends:
                        dict_['prepend'] += 1
                        something_found = True
                    if std_field.taggings:
                        dict_['tagging'] += 1
                        something_found = True

            elif isinstance(std_comm, StdComm):
                if std_comm.localprefs:
                    dict_['localpref'] += 1
                    something_found = True
                if std_comm.announcements:
                    dict_['announcement'] += 1
                    something_found = True
                if std_comm.prepends:
                    dict_['prepend'] += 1
                    something_found = True
                if std_comm.taggings:
                    dict_['tagging'] += 1
                    something_found = True

            if something_found:
                break

        if not something_found:
            dict_['nothing'] += 1

        return something_found

    def _get_scope_tup(self, scope_id):
        global STATISTICS3_GEOGRAPHIC_DICT
        global STATISTICS3_IXP_DICT
        global STATISTICS3_PEERTYPE_DICT
        global STATISTICS3_ASES_DICT

        if scope_id in STATISTICS3_GEOGRAPHIC_DICT:
            return 'geo', STATISTICS3_GEOGRAPHIC_DICT[scope_id]

        if scope_id in STATISTICS3_IXP_DICT:
            return 'ixp', str(STATISTICS3_IXP_DICT[scope_id])

        if scope_id in STATISTICS3_PEERTYPE_DICT:
            return 'peer', str(STATISTICS3_PEERTYPE_DICT[scope_id])

        if scope_id in STATISTICS3_ASES_DICT:
            return 'ases', STATISTICS3_ASES_DICT[scope_id]

        return None

    def _count_taxonomies_sets(self, std_comm_lst: [], dict_: {}, std_value: StdValue):

        global STATISTICS3_LOCALPREF_DICT
        global STATISTICS3_ANNOUNCEMENT_DICT
        global STATISTICS3_PREPEND_DICT
        global STATISTICS3_TAGGING_DICT
        global STATISTICS3_ASES_DICT
        global STATISTICS3_COMM2TAXO
        global STATISTICS3_TAXO2COMM

        if not std_comm_lst:
            return False

        something_found = False

        for std_comm in std_comm_lst:

            if isinstance(std_comm, StdCommFieldValue):
                for std_field in std_comm.fields:

                    if std_field.sgk in STATISTICS3_LOCALPREF_DICT:
                        scope_id = STATISTICS3_LOCALPREF_DICT[std_field.sgk]
                        dict_['localpref'] += 1
                        STATISTICS3_TAXO2COMM['localpref'].add(std_value.value)
                        tmp = STATISTICS3_COMM2TAXO.get(std_value.value, set())
                        tmp.add(('localpref', self._get_scope_tup(scope_id)))
                        STATISTICS3_COMM2TAXO[std_value.value] = tmp
                        something_found = True

                    if std_field.sgk in STATISTICS3_ANNOUNCEMENT_DICT:
                        (scope_id, skip, blackhole) = STATISTICS3_ANNOUNCEMENT_DICT[std_field.sgk]
                        if not skip and not blackhole:
                            dict_['do announce'] += 1
                            STATISTICS3_TAXO2COMM['do announce'].add(std_value.value)
                            tmp = STATISTICS3_COMM2TAXO.get(std_value.value, set())
                            tmp.add(('do announce', self._get_scope_tup(scope_id)))
                            STATISTICS3_COMM2TAXO[std_value.value] = tmp

                        elif skip and not blackhole:
                            dict_['do not announce'] += 1
                            STATISTICS3_TAXO2COMM['do not announce'].add(std_value.value)
                            tmp = STATISTICS3_COMM2TAXO.get(std_value.value, set())
                            tmp.add(('do not announce', self._get_scope_tup(scope_id)))
                            STATISTICS3_COMM2TAXO[std_value.value] = tmp

                        else:
                            dict_['blackhole'] += 1
                            STATISTICS3_TAXO2COMM['blackhole'].add(std_value.value)
                            tmp = STATISTICS3_COMM2TAXO.get(std_value.value, set())
                            tmp.add(('blackhole', self._get_scope_tup(scope_id)))
                            STATISTICS3_COMM2TAXO[std_value.value] = tmp

                        something_found = True

                    if std_field.sgk in STATISTICS3_PREPEND_DICT:
                        scope_id = STATISTICS3_PREPEND_DICT[std_field.sgk]
                        dict_['prepend'] += 1
                        STATISTICS3_TAXO2COMM['prepend'].add(std_value.value)
                        tmp = STATISTICS3_COMM2TAXO.get(std_value.value, set())
                        tmp.add(('prepend', self._get_scope_tup(scope_id)))
                        STATISTICS3_COMM2TAXO[std_value.value] = tmp
                        something_found = True

                    if std_field.sgk in STATISTICS3_TAGGING_DICT:
                        scope_id = STATISTICS3_TAGGING_DICT[std_field.sgk]
                        dict_['tagging'] += 1
                        STATISTICS3_TAXO2COMM['tagging'].add(std_value.value)
                        tmp = STATISTICS3_COMM2TAXO.get(std_value.value, set())
                        tmp.add(('tagging', self._get_scope_tup(scope_id)))
                        STATISTICS3_COMM2TAXO[std_value.value] = tmp
                        something_found = True

            elif isinstance(std_comm, StdComm):
                if std_comm.sgk in STATISTICS3_LOCALPREF_DICT:
                    scope_id = STATISTICS3_LOCALPREF_DICT[std_comm.sgk]
                    dict_['localpref'] += 1
                    STATISTICS3_TAXO2COMM['localpref'].add(std_value.value)
                    tmp = STATISTICS3_COMM2TAXO.get(std_value.value, set())
                    tmp.add(('localpref', self._get_scope_tup(scope_id)))
                    STATISTICS3_COMM2TAXO[std_value.value] = tmp
                    something_found = True

                if std_comm.sgk in STATISTICS3_ANNOUNCEMENT_DICT:
                    (scope_id, skip, blackhole) = STATISTICS3_ANNOUNCEMENT_DICT[std_comm.sgk]
                    if not skip and not blackhole:
                        dict_['do announce'] += 1
                        STATISTICS3_TAXO2COMM['do announce'].add(std_value.value)
                        tmp = STATISTICS3_COMM2TAXO.get(std_value.value, set())
                        tmp.add(('do announce', self._get_scope_tup(scope_id)))
                        STATISTICS3_COMM2TAXO[std_value.value] = tmp

                    elif skip and not blackhole:
                        dict_['do not announce'] += 1
                        STATISTICS3_TAXO2COMM['do not announce'].add(std_value.value)
                        tmp = STATISTICS3_COMM2TAXO.get(std_value.value, set())
                        tmp.add(('do not announce', self._get_scope_tup(scope_id)))
                        STATISTICS3_COMM2TAXO[std_value.value] = tmp

                    else:
                        dict_['blackhole'] += 1
                        STATISTICS3_TAXO2COMM['blackhole'].add(std_value.value)
                        tmp = STATISTICS3_COMM2TAXO.get(std_value.value, set())
                        tmp.add(('blackhole', self._get_scope_tup(scope_id)))
                        STATISTICS3_COMM2TAXO[std_value.value] = tmp

                    something_found = True

                if std_comm.sgk in STATISTICS3_PREPEND_DICT:
                    scope_id = STATISTICS3_PREPEND_DICT[std_comm.sgk]
                    dict_['prepend'] += 1
                    STATISTICS3_TAXO2COMM['prepend'].add(std_value.value)
                    tmp = STATISTICS3_COMM2TAXO.get(std_value.value, set())
                    tmp.add(('prepend', self._get_scope_tup(scope_id)))
                    STATISTICS3_COMM2TAXO[std_value.value] = tmp
                    something_found = True

                if std_comm.sgk in STATISTICS3_TAGGING_DICT:
                    scope_id = STATISTICS3_TAGGING_DICT[std_comm.sgk]
                    dict_['tagging'] += 1
                    STATISTICS3_TAXO2COMM['tagging'].add(std_value.value)
                    tmp = STATISTICS3_COMM2TAXO.get(std_value.value, set())
                    tmp.add(('tagging', self._get_scope_tup(scope_id)))
                    STATISTICS3_COMM2TAXO[std_value.value] = tmp
                    something_found = True

            if something_found:
                break

        if not something_found:
            dict_['nothing'] += 1

        return something_found

    def _scope_count(self, scope_id, as_path_tup, dict_):
        global STATISTICS3_GEOGRAPHIC_DICT
        global STATISTICS3_IXP_DICT
        global STATISTICS3_PEERTYPE_DICT
        global STATISTICS3_ASES_DICT

        if scope_id in STATISTICS3_GEOGRAPHIC_DICT:
            tmp_dict = dict_['geo'].get(
                STATISTICS3_GEOGRAPHIC_DICT[scope_id], dict())
            tmp = tmp_dict.get(as_path_tup, 0)
            tmp += 1
            tmp_dict[as_path_tup] = tmp
            dict_['geo'][STATISTICS3_GEOGRAPHIC_DICT[scope_id]] = tmp_dict

        if scope_id in STATISTICS3_IXP_DICT:
            tmp_dict = dict_['ixp'].get(
                str(STATISTICS3_IXP_DICT[scope_id]), dict())
            tmp = tmp_dict.get(as_path_tup, 0)
            tmp += 1
            tmp_dict[as_path_tup] = tmp
            dict_['ixp'][str(STATISTICS3_IXP_DICT[scope_id])] = tmp_dict

        if scope_id in STATISTICS3_PEERTYPE_DICT:
            tmp_dict = dict_['peer'].get(
                str(STATISTICS3_PEERTYPE_DICT[scope_id]), dict())
            tmp = tmp_dict.get(as_path_tup, 0)
            tmp += 1
            tmp_dict[as_path_tup] = tmp
            dict_['peer'][str(STATISTICS3_PEERTYPE_DICT[scope_id])] = tmp_dict

        if scope_id in STATISTICS3_ASES_DICT:
            tmp_dict = dict_['ases'].get(
                STATISTICS3_ASES_DICT[scope_id], dict())
            tmp = tmp_dict.get(as_path_tup, 0)
            tmp += 1
            tmp_dict[as_path_tup] = tmp
            dict_['ases'][STATISTICS3_ASES_DICT[scope_id]] = tmp_dict

    def _taxonomy_relationship(self, std_comm_list, as_path_tup):

        global STATISTICS3_LOCALPREF_DICT
        global STATISTICS3_ANNOUNCEMENT_DICT
        global STATISTICS3_PREPEND_DICT
        global STATISTICS3_TAGGING_DICT
        global STATISTICS3_SCOPE_COUNT

        dict_ = STATISTICS3_SCOPE_COUNT

        if not std_comm_list:
            return False

        something_found = False

        for std_comm in std_comm_list:

            if isinstance(std_comm, StdCommFieldValue):
                for std_field in std_comm.fields:

                    if std_field.sgk in STATISTICS3_LOCALPREF_DICT:
                        self._scope_count(
                            STATISTICS3_LOCALPREF_DICT[std_field.sgk],
                            as_path_tup,
                            dict_['localpref'])
                        something_found = True

                    if std_field.sgk in STATISTICS3_ANNOUNCEMENT_DICT:
                        (scope_id, skip, blackhole) = STATISTICS3_ANNOUNCEMENT_DICT[std_field.sgk]
                        if not skip and not blackhole:
                            self._scope_count(
                                scope_id,
                                as_path_tup,
                                dict_['do announce'])

                        elif skip and not blackhole:
                            self._scope_count(
                                scope_id,
                                as_path_tup,
                                dict_['do not announce'])

                        else:
                            self._scope_count(
                                scope_id,
                                as_path_tup,
                                dict_['blackhole'])

                        something_found = True

                    if std_field.sgk in STATISTICS3_PREPEND_DICT:
                        self._scope_count(
                            STATISTICS3_PREPEND_DICT[std_field.sgk],
                            as_path_tup,
                            dict_['prepend'])
                        something_found = True
                    if std_field.sgk in STATISTICS3_TAGGING_DICT:
                        self._scope_count(
                            STATISTICS3_TAGGING_DICT[std_field.sgk],
                            as_path_tup,
                            dict_['tagging'])
                        something_found = True

            elif isinstance(std_comm, StdComm):
                if std_comm.sgk in STATISTICS3_LOCALPREF_DICT:
                    self._scope_count(
                        STATISTICS3_LOCALPREF_DICT[std_comm.sgk],
                        as_path_tup,
                        dict_['localpref'])
                    something_found = True

                if std_comm.sgk in STATISTICS3_ANNOUNCEMENT_DICT:
                    (scope_id, skip, blackhole) = STATISTICS3_ANNOUNCEMENT_DICT[std_comm.sgk]
                    if not skip and not blackhole:
                        self._scope_count(
                            scope_id,
                            as_path_tup,
                            dict_['do announce'])

                    elif skip and not blackhole:
                        self._scope_count(
                            scope_id,
                            as_path_tup,
                            dict_['do not announce'])

                    else:
                        self._scope_count(
                            scope_id,
                            as_path_tup,
                            dict_['blackhole'])

                    something_found = True

                if std_comm.sgk in STATISTICS3_PREPEND_DICT:
                    self._scope_count(
                        STATISTICS3_PREPEND_DICT[std_comm.sgk],
                        as_path_tup,
                        dict_['prepend'])
                    something_found = True

                if std_comm.sgk in STATISTICS3_TAGGING_DICT:
                    self._scope_count(
                        STATISTICS3_TAGGING_DICT[std_comm.sgk],
                        as_path_tup,
                        dict_['tagging'])
                    something_found = True

            if something_found:
                break

        return something_found

    def _statistics3_single_dump(self, dump_file):

        global STATISTICS3_NUMBER_EXT_COMM
        global STATISTICS3_COMM_TAXO_AMOUNT
        global STATISTICS3_SCOPE_COUNT
        global STATISTICS3_NEIGHBOUR_RESERVED_DISTANCE_DICT

        global STATISTICS3_LOCALPREF_DICT
        global STATISTICS3_ANNOUNCEMENT_DICT
        global STATISTICS3_PREPEND_DICT
        global STATISTICS3_TAGGING_DICT
        global STATISTICS3_GEOGRAPHIC_DICT
        global STATISTICS3_IXP_DICT
        global STATISTICS3_PEERTYPE_DICT
        global STATISTICS3_ASES_DICT

        global OREGON_NEIGHBOUR_ASES
        global OREGON_NEIGHBOUR_DOUBLE_PEER_PER_AS_SECOND_INDEXES

        tmp = self.session.query(StdComm.LocalPref.std_comm_id,
                                 StdComm.LocalPref._scope_id).all()
        STATISTICS3_LOCALPREF_DICT = dict(tmp)
        print("STATISTICS3_LOCALPREF_DICT", len(STATISTICS3_LOCALPREF_DICT))

        tmp = self.session.query(StdComm.Announcement.std_comm_id,
                                 StdComm.Announcement._scope_id,
                                 StdComm.Announcement.skip,
                                 StdComm.Announcement.blackhole, ).all()
        STATISTICS3_ANNOUNCEMENT_DICT = dict((x, (y1, y2, y3)) for (x, y1, y2, y3) in tmp)
        print("STATISTICS3_ANNOUNCEMENT_DICT",
              len(STATISTICS3_ANNOUNCEMENT_DICT))

        tmp = self.session.query(StdComm.Prepend.std_comm_id,
                                 StdComm.Prepend._scope_id, ).all()
        STATISTICS3_PREPEND_DICT = dict(tmp)
        print("STATISTICS3_PREPEND_DICT", len(STATISTICS3_PREPEND_DICT))

        tmp = self.session.query(StdComm.Tagging.std_comm_id,
                                 StdComm.Tagging._scope_id).all()
        STATISTICS3_TAGGING_DICT = dict(tmp)
        print("STATISTICS3_TAGGING_DICT", len(STATISTICS3_TAGGING_DICT))

        tmp = self.session.query(Geographic.sgk,
                                 Geographic._continent,
                                 Geographic._country).all()
        STATISTICS3_GEOGRAPHIC_DICT = dict((x, (y1, y2)) for (x, y1, y2) in tmp)
        print("STATISTICS3_GEOGRAPHIC_DICT", len(STATISTICS3_GEOGRAPHIC_DICT))

        tmp = self.session.query(Ixp.sgk, Ixp.name).all()
        STATISTICS3_IXP_DICT = dict(tmp)
        print("STATISTICS3_IXP_DICT", len(STATISTICS3_IXP_DICT))

        tmp = self.session.query(Peertype.sgk, Peertype.type_).all()
        STATISTICS3_PEERTYPE_DICT = dict(tmp)
        print("STATISTICS3_PEERTYPE_DICT", len(STATISTICS3_PEERTYPE_DICT))

        tmp = self.session.query(Ases.sgk, Ases.asn).all()
        STATISTICS3_ASES_DICT = dict(tmp)
        print("STATISTICS3_ASES_DICT", len(STATISTICS3_ASES_DICT))

        cache = FixTimeStdValuesCache(self.session)

        reader = mp.Reader(dump_file)

        # FIXME WARNING hard coded value for special dump
        max_entries = 21365118

        # for m in reader:
        #     m = m.mrt
        #     if m.type == mp.MsgT.table_dump:
        #         raise Exception("Type TABLE_DUMP unexpected.")
        #     elif m.type == mp.MsgT.table_dump_v2:
        #         if m.subtype == mp.TdV2St.peer_index_table:
        #             pass
        #         elif (m.subtype == mp.TdV2St.rib_ipv4_unicast
        #               or m.subtype == mp.TdV2St.rib_ipv4_multicast
        #               or m.subtype == mp.TdV2St.rib_ipv6_unicast
        #               or m.subtype == mp.TdV2St.rib_ipv6_multicast):

        #             for entry in m.rib.entry:
        #                 max_entries += 1

        reader = mp.Reader(dump_file)

        cur_entry = 0
        neighbour_reserved_distance_examples = []

        for i, m in enumerate(reader):
            m = m.mrt

            if m.type == mp.MsgT.table_dump:
                raise Exception("Type TABLE_DUMP unexpected.")
            elif m.type == mp.MsgT.table_dump_v2:
                if m.subtype == mp.TdV2St.peer_index_table:
                    pass
                elif (m.subtype == mp.TdV2St.rib_ipv4_unicast
                      or m.subtype == mp.TdV2St.rib_ipv4_multicast
                      or m.subtype == mp.TdV2St.rib_ipv6_unicast
                      or m.subtype == mp.TdV2St.rib_ipv6_multicast):

                    for j, entry in enumerate(m.rib.entry):
                        cur_entry += 1
                        communities_set = set()
                        as_path_merged = []
                        attr_as_path = None

                        for attr in entry.attr:

                            if attr.type == mp.BgpAttrT.community:
                                communities_set.update(attr.comm)

                            elif attr.type == mp.BgpAttrT.as_path:
                                attr_as_path = attr
                                for seg in attr.as_path:

                                    if seg[
                                        'type'] == mp.AsPathSegT.as_sequence:
                                        as_path_merged = (
                                            as_path_merged +
                                            list(map(
                                                int, seg['val'].split(' '))))
                                    elif seg['type'] == mp.AsPathSegT.as_set:
                                        as_path_merged.append(
                                            frozenset(map(
                                                int, seg['val'].split(' '))))
                                    else:
                                        print(
                                            "WARN unexpected AsPathSegT type")

                        as_path_list = []
                        if isinstance(as_path_merged[-1], (set, frozenset)):
                            for orig in as_path_merged[-1]:
                                tmp = as_path_merged[:-1]
                                tmp.append(orig)
                                as_path_list.append(tuple(tmp))
                        else:
                            as_path_list.append(tuple(as_path_merged))

                        std_values_list = []
                        for comm in communities_set:
                            std_values_list.append(StdValue(comm))

                        for as_path in as_path_list:
                            neigbour_as = int(as_path[0])

                            if entry.peer_index in OREGON_NEIGHBOUR_DOUBLE_PEER_PER_AS_SECOND_INDEXES:
                                neigbour_as_signed = -neigbour_as
                            else:
                                neigbour_as_signed = neigbour_as

                            sec_neighbour_as_set = set()
                            try:
                                if isinstance(as_path[1], (set, frozenset)):
                                    for asn_ in as_path[1]:
                                        sec_neighbour_as_set.add(int(asn_))
                                else:
                                    sec_neighbour_as_set.add(int(as_path[1]))
                            except IndexError as _:
                                pass

                            # STATISTICS3_NEIGHBOUR_RESERVED_DISTANCE_DICT
                            (am_neighbour_defined_by, am_other_defined_by, am_is_not_in_as_path, am_not_in_db
                             ) = STATISTICS3_NEIGHBOUR_RESERVED_DISTANCE_DICT.get(neigbour_as_signed, (0, 0, 0, 0))

                            for std_value in std_values_list:
                                outp = ("%s/%s (%s%%)" % (
                                    cur_entry, max_entries,
                                    round((cur_entry * 100 / max_entries), 2)))
                                print(std_value, outp)

                                # STATISTICS3_NEIGHBOUR_RESERVED_DISTANCE_DICT
                                has_neighbour_definded_by = False
                                has_other_defined_by = False
                                is_not_in_as_path = False
                                defined_by_set = set()

                                std_comm_lst = cache.get_std_comm(
                                    std_value,
                                    # TODO options = [load_only('sgk')],
                                    # TODO field_load_only=['sgk'],
                                )

                                if std_comm_lst:
                                    self._count_taxonomies_sets(
                                        std_comm_lst,
                                        STATISTICS3_COMM_TAXO_AMOUNT,
                                        std_value)
                                    entry_found = False
                                    for comm in std_comm_lst:
                                        try:
                                            idx = as_path.index(comm.asn)
                                            tup = (idx, len(as_path))

                                            # this maybe can success multiple times. if an private comm
                                            # defined in multiple AS we don't know which one has it added.
                                            # But I don't expect that this happens often. Private comms
                                            # especially if you have defined the same should be removed
                                            # from the AS when received the route
                                            # Theoretically multiple AS can also define the same community
                                            # multiple times, but this should also not happen.
                                            entry_found = True
                                        except ValueError as _:
                                            pass

                                        # STATISTICS3_NEIGHBOUR_RESERVED_DISTANCE_DICT
                                        if std_value.is_reserved:
                                            as_path_int_no_neighbour = as_path[1:]
                                            as_path_int_no_neighbour_flat = []
                                            for a in as_path_int_no_neighbour:
                                                if isinstance(a, (set, frozenset)):
                                                    as_path_int_no_neighbour_flat.extend(a)
                                                else:
                                                    as_path_int_no_neighbour_flat.append(a)
                                            defined_by_set.add(comm.asn)

                                            if comm.asn == neigbour_as:
                                                has_neighbour_definded_by = True
                                            elif comm.asn in as_path_int_no_neighbour_flat:
                                                has_other_defined_by = True
                                            else:
                                                is_not_in_as_path = True

                                    if not entry_found:
                                        if std_value.is_reserved:
                                            # more specifially: private and not in DB or in DB but not in AS-Path
                                            tup = ('reserved', len(as_path))
                                        else:
                                            tup = ('error', len(as_path))
                                    self._taxonomy_relationship(
                                        std_comm_lst, tup)
                                else:
                                    STATISTICS3_COMM_TAXO_AMOUNT['unknown'] += 1


                                # STATISTICS3_NEIGHBOUR_RESERVED_DISTANCE_DICT
                                if has_neighbour_definded_by:
                                    am_neighbour_defined_by += 1
                                elif has_other_defined_by:
                                    am_other_defined_by += 1
                                    neighbour_reserved_distance_examples.append(
                                        (std_value.str_value, defined_by_set, as_path))
                                elif is_not_in_as_path:
                                    am_is_not_in_as_path += 1
                                else:
                                    am_not_in_db += 1

                                STATISTICS3_NEIGHBOUR_RESERVED_DISTANCE_DICT[neigbour_as_signed] = (
                                    am_neighbour_defined_by, am_other_defined_by, am_is_not_in_as_path, am_not_in_db)


                                # elif attr.type == mp.BgpAttrT.as_path:
                                #    # TODO

                    print(STATISTICS3_COMM_TAXO_AMOUNT)

            elif (m.type == mp.MsgT.bgp4mp or m.type == mp.MsgT.bgp4mp_et):
                pass

        for e in neighbour_reserved_distance_examples:
            print(e)