import pickle


def update_func(self, other, func):
    for k, v in other.items():
        try:
            self[k] = func(self[k], v)
        except KeyError as _:
            self[k] = v


def pos_len_tup_to_list(dict_):
    lst = []
    for (p, l), v in dict_.items():
        if isinstance(p, str):
            continue
        percent = p/l
        for i in range(v):
            lst.append(percent)

    return lst


def scope_dict(dict_):
    scope_list = []
    data_list = []

    scope_list.append("geographic")
    data_list.append(pos_len_tup_to_list(dict_['geo']))

    scope_list.append("IXP")
    tmp_dict = dict()
    for k, v in dict_['ixp'].items():
        update_func(tmp_dict, v, lambda x, y: sum([x, y]))

    data_list.append(pos_len_tup_to_list(tmp_dict))

    for k, v in dict_['peer'].items():
        scope_list.append(k)
        data_list.append(pos_len_tup_to_list(v))

    return (scope_list, data_list)


def main():
    inputfile = "/media/osshare-crypt/routeviews2/p3-single-stat3/p3-single-stat3-scope-corr.data"

    with open(inputfile, 'rb') as data_file:
        list_dicts = pickle.load(data_file)
    list_dicts = list_dicts['STATISTICS3_SCOPE_COUNT']
    for k,v in list_dicts.items():
        print(k)

    save_dict = dict()
    save_dict['announcement'] = scope_dict(list_dicts['announcement'])
    save_dict['localpref'] = scope_dict(list_dicts['localpref'])
    save_dict['prepend'] = scope_dict(list_dicts['prepend'])
    save_dict['tagging'] = scope_dict(list_dicts['tagging'])
    
    dumpfile = "/media/osshare-crypt/routeviews2/p3-single-stat3/p3-single-stat3-scope-corr-process1.data"
    print("Writing pickle file to", dumpfile)
    with open(dumpfile, 'wb') as data_file:
        pickle.dump(save_dict, data_file, pickle.HIGHEST_PROTOCOL)

if __name__ == '__main__':
    main()
